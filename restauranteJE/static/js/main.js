// main.js
function mostrarFormulario() {
    var formulario = document.getElementById('formulario');
    formulario.style.display = 'block';
}

function ocultarFormulario() {
    var formulario = document.getElementById('formulario');
    formulario.style.display = 'none';
}

//---------provincia----

function eliminarProvincia(url){
    iziToast.question({
        timeout: 15000,
        close: true,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: 'Confirmacion',
        message: 'Esta seguro de eliminar la provincia seleccionado',
        position: 'center',
        buttons: [
            ['<button><b>SI</b></button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                window.location.href=url;
            }, true],
            ['<button>NO</button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ],
        onClosing: function(instance, toast, closedBy){
            console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        }
    });
  }

$(document).ready(function () {
     
    $.validator.addMethod("letterswithspaces", function (value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Ingrese solo texto y espacios");

    $('#tbl_provincias').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'Reporte de Provincias '
            },
            'print',
            'csv'
        ]
    });

    $("#frm_nuevo_provincia").validate({
        
        rules: {
            "nombre_je": {
                required:true,
                letterswithspaces:true
            },
            "capital_je": { 
                required:true,
                letterswithspaces:true
            },
            "imagen_je": { 
                required:true 
            }
        },
        messages: {
            "nombre_je": { 
                required: "Ingrese la provincia",
                letterswithspaces:"ingrese solo texto"
            },
            "capital_je": { 
                required: "Ingrese la capital",
                letterswithspaces:"ingrese solo texto"
            },
            "imagen_je": { required: "Ingrese Fotografia de la provincia" }
        }
    });
});

//------- CLIENTE-----
function eliminarCliente(url){
    iziToast.question({
        timeout: 15000,
        close: true,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: 'Confirmacion',
        message: 'Esta seguro de eliminar el cliente seleccionado',
        position: 'center',
        buttons: [
            ['<button><b>SI</b></button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                window.location.href=url;
            }, true],
            ['<button>NO</button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ],
        onClosing: function(instance, toast, closedBy){
            console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        }
    });
  }

$(document).ready(function () {
     
    $.validator.addMethod("letterswithspaces", function (value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Ingrese solo texto y espacios");

    $('#tbl_clientes').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'Reporte de Clientes '
            },
            'print',
            'csv'
        ]
    });

    $("#frm_nuevo_cliente").validate({
        
        rules: {
            "idP_je":{required:true },
            "cedula_je":{
                required:true,
                minlength:10,
                maxlength:10
            },
            "apellido_je": {
                required:true,
                letterswithspaces:true
            },
            "nombre_je": { 
                required:true,
                letterswithspaces:true
            },
            "telefono_je": {
                required:true,
                maxlength:10,
                minlength:10
            },
            "correo_je":{
                required:true,
                email:true
            }
        },
        messages: {
            "idP_je":{required:"Seleccione la Provincia del cliente " },
            "cedula_je":{
                required:"Ingrese el numero de cedula del Cliente",
                minlength:"La cedula no debe tener menos de 10 digitos",
                maxlength:"La cedula no debe tener mas de 10 digitos"
            },
            "apellido_je":{
                required:"Ingrese el Apellido del cliente",
                letterswithspaces:"ingrese solo texto"
            },    
            "nombre_je":{
                required:"Ingrese el Nombre del cliente",
                letterswithspaces:"ingrese solo texto"
            },
            "telefono_je": {
                required:"Ingrese el Telefono del cliente",
                maxlength:"El numero de telefono no debe tener mas de 10 digitos",
                minlength:"El numero de telefono  no debe tener menos de 10 digitos"
            },
            "correo_je":{
                required:"Ingrese el correo electronico del Cliente",
                email:"Ingrese un correo electronico valido"
            }
        }
    });
});

///--------pedido
function eliminarPedido(url){
    iziToast.question({
        timeout: 15000,
        close: true,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: 'Confirmacion',
        message: 'Esta seguro de eliminar el Pedido seleccionado',
        position: 'center',
        buttons: [
            ['<button><b>SI</b></button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                window.location.href=url;
            }, true],
            ['<button>NO</button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ],
        onClosing: function(instance, toast, closedBy){
            console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        }
    });
  }

$(document).ready(function () {
       
    $('#tbl_pedidos').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'Reporte de Pedidos '
            },
            'print',
            'csv'
        ]
    });

    $("#frm_nuevo_pedido").validate({
        rules: {
            "idCl_je":{required:true },
            "fecha_je":{required:true}            
        },
        messages: {
            "idCl_je":{required:"Seleccione  cliente " },
            "fecha_je":{required:"Ingrese la fecha"}
            
        }
    });
});  

///--------tipo
function eliminarTipo(url){
    iziToast.question({
        timeout: 15000,
        close: true,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: 'Confirmacion',
        message: 'Esta seguro de eliminar tipo de platillo',
        position: 'center',
        buttons: [
            ['<button><b>SI</b></button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                window.location.href=url;
            }, true],
            ['<button>NO</button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ],
        onClosing: function(instance, toast, closedBy){
            console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        }
    });
  }

$(document).ready(function () {
     
    $.validator.addMethod("letterswithspaces", function (value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Ingrese solo texto y espacios");

    $('#tbl_tipos').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'Reporte de tipo '
            },
            'print',
            'csv'
        ]
    });

    $("#frm_nuevo_tipo").validate({
        
        rules: {
            "tipo_je": {
                required:true,
                letterswithspaces:true
            },          
            "imagen_je": { 
                required:true 
            }
        },
        messages: {
            "tipo_je": { 
                required: "Ingrese el el tipo de platillo",
                letterswithspaces:"ingrese solo texto"
            },
            "imagen_je": { required: "Ingrese Fotografia de la provincia" }
        }
    });
});

//--------platillo
function eliminarPlatillo(url){
    iziToast.question({
        timeout: 15000,
        close: true,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: 'Confirmacion',
        message: 'Esta seguro de eliminar el platillo',
        position: 'center',
        buttons: [
            ['<button><b>SI</b></button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                window.location.href=url;
            }, true],
            ['<button>NO</button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ],
        onClosing: function(instance, toast, closedBy){
            console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        }
    });
  }

$(document).ready(function () {
     
    $.validator.addMethod("letterswithspaces", function (value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Ingrese solo texto y espacios");
    $.validator.addMethod("validarPrecio", function (value, element) {
        return /^\d+(\.\d{1,2})?$/.test(value);
    }, "Ingrese un precio válido");
    

    $('#tbl_platillos').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'Reporte de Platillo '
            },
            'print',
            'csv'
        ]
    });

    $("#frm_nuevo_platillo").validate({
        
        rules: {
            "idT_je":{required:true },
            "nombre_je": { 
                required:true,
                letterswithspaces:true
            },
            "descripcion_je": {
                required:true
            },
            "precio_je":{
                required:true,
                validarPrecio:true          
            },
            "imagen_je": { 
                required:true 
            }
        },
        messages: {
            "idT_je":{required:"Seleccione el tipo de platillo" },            
            "nombre_je":{
                required:"Ingrese el Nombre del platillo",
                letterswithspaces:"ingrese solo texto"
            },
            "descripcion_je": {
                required:"Ingrese descripcion del platillo"
            },
            "precio_je":{
                required:"Ingrese precio",
                validarPrecio:"Ingrese un Precio valido"            
            },
            "imagen_je": { 
                required:"Ingrese Fotografia " 
            }
        }
    });
});

//--------detalle
function eliminarDetalle(url){
    iziToast.question({
        timeout: 15000,
        close: true,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: 'Confirmacion',
        message: 'Esta seguro de eliminar el detalle',
        position: 'center',
        buttons: [
            ['<button><b>SI</b></button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                window.location.href=url;
            }, true],
            ['<button>NO</button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ],
        onClosing: function(instance, toast, closedBy){
            console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        }
    });
  }

$(document).ready(function () {
     
    $.validator.addMethod("letterswithspaces", function (value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Ingrese solo texto y espacios");
    $.validator.addMethod("validarPrecio", function (value, element) {
        return /^\d+(\.\d{1,2})?$/.test(value);
    }, "Ingrese un precio válido");
    $.validator.addMethod("validarNumeroEntero", function (value, element) {
        return /^\d+$/.test(value);
    }, "Ingrese un número entero");

    $('#tbl_detalles').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'Reporte de Detalle '
            },
            'print',
            'csv'
        ]
    });

    $("#frm_nuevo_detalle").validate({
        
        rules: {
            "idPl_je":{required:true },
            "idPe_je":{required:true },
            "cantidad_je": { 
                required:true,
                validarNumeroEntero:true
            },
            "descripcion_je": {
                required:true
            },
            "total_je":{
                required:true,
                validarPrecio:true            
            }
        },
        messages: {
            "idPl_je":{required:"Seleccione el platillo" },            
            "idPe_je":{required:"Seleccione el pedido" },            
    
            "cantidad_je":{
                required:"Ingrese cantidad  del platillo",
                validarNumeroEntero:"ingrese numeros enteros"
            },
            "descripcion_je": {
                required:"Ingrese descripcion"
            },
            "total_je":{
                required:"Ingrese precio  total",
                validarPrecio:"Ingrese un precio valido"              
            }
        }
    });
});

//-------ingrediente

function eliminarIngrediente(url){
    iziToast.question({
        timeout: 15000,
        close: true,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: 'Confirmacion',
        message: 'Esta seguro de eliminar el  ingrediente  seleccionado',
        position: 'center',
        buttons: [
            ['<button><b>SI</b></button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                window.location.href=url;
            }, true],
            ['<button>NO</button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ],
        onClosing: function(instance, toast, closedBy){
            console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        }
    });
  }

$(document).ready(function () {
     
    $.validator.addMethod("letterswithspaces", function (value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Ingrese solo texto y espacios");
    

    $('#tbl_ingredientes').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'Reporte de Ingredientes '
            },
            'print',
            'csv'
        ]
    });

    $("#frm_nuevo_ingrediente").validate({
        
        rules: {
            "nombre_je": {
                required:true,
                letterswithspaces:true
            },
            "descripcion_je": { 
                required:true
            },
            "fecha_caducidad_je": { 
                required:true
            },
            "cantidad_je": { 
                required:true
            },
            "imagen_je": { 
                required:true 
            }
        },
        messages: {
            "nombre_je": { 
                required: "Ingrese nombre del ingrediente ",
                letterswithspaces:"ingrese solo texto"
            },
            "descripcion_je": { 
                required: "Ingrese descripcion"
            },
            "fecha_caducidad_je": { 
                required:"ingree fecha "
            },
            "cantidad_je": { 
                required:"iingresse cantidad"
            },
            "imagen_je": { required: "Ingrese Fotografia del ingrediente " }
        }
    });
});

//--------receta
function eliminarReceta(url){
    iziToast.question({
        timeout: 15000,
        close: true,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: 'Confirmacion',
        message: 'Esta seguro de eliminar la receta',
        position: 'center',
        buttons: [
            ['<button><b>SI</b></button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                window.location.href=url;
            }, true],
            ['<button>NO</button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ],
        onClosing: function(instance, toast, closedBy){
            console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        }
    });
  }

$(document).ready(function () {
     
    $.validator.addMethod("letterswithspaces", function (value, element) {
        return /^[a-zA-Z\s]+$/.test(value);
    }, "Ingrese solo texto y espacios");
    $.validator.addMethod("validarNumeroEntero", function (value, element) {
        return /^\d+$/.test(value);
    }, "Ingrese un número entero");
    

    $('#tbl_recetas').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'Reporte de receta '
            },
            'print',
            'csv'
        ]
    });

    $("#frm_nuevo_receta").validate({
        
        rules: {
            "idPl_je":{required:true },
            "idI_je":{required:true },
            "preparacion_je": { 
                required:true
            },
            "dificultad_je": {
                required:true,
                letterswithspaces:true
            },
            "porciones_je":{
                required:true,
                validarNumeroEntero:true
                          
            }
        },
        messages: {
            "idPl_je":{required:"Seleccione el platillo" },            
            "idI_je":{required:"Seleccione el pedido" },            
    
            "preparacion_je":{
                required:"Ingrese las instrucciones"
            },
            "dificultad_je": {
                required:"Ingrese dificultad (media. alta, baja)",
                letterswithspaces:"ingrese solo texto"
            },
            "porciones_je":{
                required:"Ingrese las porciones",
                validarNumeroEntero:"ingrese numeros enteros "
            }
        }
    });
});
