from django.shortcuts import render,redirect
from .models import Provincia, Cliente, Pedido, Tipo, Platillo, Detalle, Ingrediente, Receta
from django.contrib import messages

# index
def index(request):
    return render(request, 'index.html')

#-------PROVINCIA----------

def listadoProvincia(request):
    provinciasBdd=Provincia.objects.all()
    return render(request, 'listadoProvincia.html',{'provincias':provinciasBdd})

def guardarProvincia(request):
    
    nombre_je=request.POST["nombre_je"]
    capital_je=request.POST["capital_je"]
    imagen_je=request.FILES.get("imagen_je")
    nuevoProvincia=Provincia.objects.create(nombre_je=nombre_je,
    capital_je=capital_je,imagen_je=imagen_je)
    messages.success(request,'Provincia guardado correctamente')
    return redirect('listadoProvincia')

def eliminarProvincia(request,idP_je):
    provinciaEliminar=Provincia.objects.get(idP_je=idP_je)
    provinciaEliminar.delete()
    messages.warning(request,'Provincia eliminado correctamente')
    return redirect('listadoProvincia')

def editarProvincia(request,idP_je):
    provinciaEditar=Provincia.objects.get(idP_je=idP_je)
    return render(request, 'editarProvincia.html',{'provincia':provinciaEditar})

def procesarActualizacionProvincia(request):
    idP_je=request.POST["idP_je"]
    nombre_je=request.POST["nombre_je"]
    capital_je=request.POST["capital_je"]
    imagen_je=request.FILES.get("imagen_je")
    #insertar datos mediante el ORM de django
    provinciaEditar=Provincia.objects.get(idP_je=idP_je)
    provinciaEditar.nombre_je=nombre_je
    provinciaEditar.capital_je=capital_je
    provinciaEditar.imagen_je=imagen_je
    provinciaEditar.save()
    messages.success(request,'Provincia actualizado correctamente')
    return redirect('listadoProvincia')

#-----------Cliente-----------
def listadoCliente(request):
    clientesBdd=Cliente.objects.all()
    provinciasBdd=Provincia.objects.all()
    return render(request, 'listadoCliente.html',{'clientes':clientesBdd, 'provincias':provinciasBdd})

def guardarCliente(request):
    idP_je=request.POST["idP_je"]
    provinciaSeleccionado=Provincia.objects.get(idP_je=idP_je)
    cedula_je=request.POST["cedula_je"]
    apellido_je=request.POST["apellido_je"]
    nombre_je=request.POST["nombre_je"]
    correo_je=request.POST["correo_je"]
    telefono_je=request.POST["telefono_je"]
    nuevoCliente=Cliente.objects.create(provincia=provinciaSeleccionado,cedula_je=cedula_je,
    apellido_je=apellido_je,nombre_je=nombre_je,correo_je=correo_je,
    telefono_je=telefono_je)
    messages.success(request,'Cliente guardado correctamente')
    return redirect('listadoCliente')

def eliminarCliente(request,idCl_je):
    clienteEliminar=Cliente.objects.get(idCl_je=idCl_je)
    clienteEliminar.delete()
    messages.warning(request,'Cliente eliminado correctamente')
    return redirect('listadoCliente')

def editarCliente(request,idCl_je):
    clienteEditar=Cliente.objects.get(idCl_je=idCl_je)
    provinciasBdd=Provincia.objects.all()
    return render(request, 'editarCliente.html',{'cliente':clienteEditar, 'provincias':provinciasBdd})

def procesarActualizacionCliente(request):
    idCl_je=request.POST["idCl_je"]
    idP_je=request.POST["idP_je"]
    provinciaSeleccionado=Provincia.objects.get(idP_je=idP_je)
    cedula_je=request.POST["cedula_je"]
    apellido_je=request.POST["apellido_je"]
    nombre_je=request.POST["nombre_je"]
    correo_je=request.POST["correo_je"]
    telefono_je=request.POST["telefono_je"]
    #insertar datos mediante el ORM de django
    clienteEditar=Cliente.objects.get(idCl_je=idCl_je)
    clienteEditar.cedula_je=cedula_je
    clienteEditar.apellido_je=apellido_je
    clienteEditar.nombre_je=nombre_je
    clienteEditar.correo_je=correo_je
    clienteEditar.telefono_je=telefono_je
    clienteEditar.provincia=provinciaSeleccionado
    clienteEditar.save()
    messages.success(request,'Cliente actualizado correctamente')
    return redirect('listadoCliente')

#------------PEDIDO-------
def listadoPedido(request):
    pedidosBdd=Pedido.objects.all()
    clientesBdd=Cliente.objects.all()
    return render(request, 'listadoPedido.html',{'pedidos':pedidosBdd, 'clientes':clientesBdd})

def guardarPedido(request):
    idCl_je=request.POST["idCl_je"]
    clienteSeleccionado=Cliente.objects.get(idCl_je=idCl_je)
    
    fecha_je=request.POST["fecha_je"]
    nuevoPedido=Pedido.objects.create(cliente=clienteSeleccionado,
    fecha_je=fecha_je)
    messages.success(request,'Pedido guardado correctamente')
    return redirect('listadoPedido')

def eliminarPedido(request,idPe_je):
    pedidoEliminar=Pedido.objects.get(idPe_je=idPe_je)
    pedidoEliminar.delete()
    messages.warning(request,'Pedido eliminado correctamente')
    return redirect('listadoPedido')

def editarPedido(request,idPe_je):
    pedidoEditar=Pedido.objects.get(idPe_je=idPe_je)
    clientesBdd=Cliente.objects.all()
    return render(request, 'editarPedido.html',{'pedido':pedidoEditar, 'clientes':clientesBdd})

def procesarActualizacionPedido(request):
    idPe_je=request.POST["idPe_je"]
    idCl_je=request.POST["idCl_je"]
    clienteSeleccionado=Cliente.objects.get(idCl_je=idCl_je)
    fecha_je=request.POST["fecha_je"]
    #insertar datos mediante el ORM de django
    pedidoEditar=Pedido.objects.get(idPe_je=idPe_je)
    pedidoEditar.fecha_je=fecha_je
    pedidoEditar.cliente=clienteSeleccionado
    pedidoEditar.save()
    messages.success(request,'Pedido actualizado correctamente')
    return redirect('listadoPedido')


#------TIPO-----
def listadoTipo(request):
    tiposBdd=Tipo.objects.all()
    return render(request, 'listadoTipo.html',{'tipos':tiposBdd})

def guardarTipo(request):  
    tipo_je=request.POST["tipo_je"]
    imagen_je=request.FILES.get("imagen_je")
    nuevoTipo=Tipo.objects.create(tipo_je=tipo_je,imagen_je=imagen_je)
    messages.success(request,'Tipo guardado correctamente')
    return redirect('listadoTipo')

def eliminarTipo(request,idT_je):
    tipoEliminar=Tipo.objects.get(idT_je=idT_je)
    tipoEliminar.delete()
    messages.warning(request,'Tipo eliminado correctamente')
    return redirect('listadoTipo')

def editarTipo(request,idT_je):
    tipoEditar=Tipo.objects.get(idT_je=idT_je)
    return render(request, 'editarTipo.html',{'tipo':tipoEditar})

def procesarActualizacionTipo(request):
    idT_je=request.POST["idT_je"]
    tipo_je=request.POST["tipo_je"]
    imagen_je=request.FILES.get("imagen_je")
    #insertar datos mediante el ORM de django
    tipoEditar=Tipo.objects.get(idT_je=idT_je)
    tipoEditar.tipo_je=tipo_je
    tipoEditar.imagen_je=imagen_je
    tipoEditar.save()
    messages.success(request,'Tipo actualizado correctamente')
    return redirect('listadoTipo')

#--------PLATILLO----

def listadoPlatillo(request):
    platillosBdd=Platillo.objects.all()
    tiposBdd=Tipo.objects.all()
    return render(request, 'listadoPlatillo.html',{'platillos':platillosBdd, 'tipos':tiposBdd})

def guardarPlatillo(request):
    idT_je=request.POST["idT_je"]
    tipoSeleccionado=Tipo.objects.get(idT_je=idT_je)
    
    nombre_je=request.POST["nombre_je"]
    descripcion_je=request.POST["descripcion_je"]
    precio_je=request.POST["precio_je"]
    imagen_je=request.FILES.get("imagen_je")
    nuevoPlatillo=Platillo.objects.create(tipo=tipoSeleccionado,
    nombre_je=nombre_je, descripcion_je=descripcion_je,
    precio_je=precio_je,imagen_je=imagen_je)
    messages.success(request,'Platillo guardado correctamente')
    return redirect('listadoPlatillo')

def eliminarPlatillo(request,idPl_je):
    platilloEliminar=Platillo.objects.get(idPl_je=idPl_je)
    platilloEliminar.delete()
    messages.warning(request,'Platillo eliminado correctamente')
    return redirect('listadoPlatillo')

def editarPlatillo(request,idPl_je):
    platilloEditar=Platillo.objects.get(idPl_je=idPl_je)
    tiposBdd=Tipo.objects.all()
    return render(request, 'editarPlatillo.html',{'platillo':platilloEditar, 'tipos':tiposBdd})

def procesarActualizacionPlatillo(request):
    idPl_je=request.POST["idPl_je"]
    idT_je=request.POST["idT_je"]
    tipoSeleccionado=Tipo.objects.get(idT_je=idT_je)
    nombre_je=request.POST["nombre_je"]
    descripcion_je=request.POST["descripcion_je"]
    precio_je=request.POST["precio_je"]
    imagen_je=request.FILES.get("imagen_je")
    #insertar datos mediante el ORM de django
    platilloEditar=Platillo.objects.get(idPl_je=idPl_je)
    platilloEditar.nombre_je=nombre_je
    platilloEditar.descripcion_je=descripcion_je
    platilloEditar.precio_je=precio_je
    platilloEditar.imagen_je=imagen_je
    platilloEditar.tipo=tipoSeleccionado
    platilloEditar.save()
    messages.success(request,'Platillo actualizado correctamente')
    return redirect('listadoPlatillo')

#--------Detalle------

def listadoDetalle(request):
    detallesBdd=Detalle.objects.all()
    pedidosBdd=Pedido.objects.all()
    platillosBdd=Platillo.objects.all()
    return render(request, 'listadoDetalle.html',{
        'detalles':detallesBdd, 
        'pedidos':pedidosBdd,
        'platillos':platillosBdd
        })

def guardarDetalle(request):
    idPe_je=request.POST["idPe_je"]
    pedidoSeleccionado=Pedido.objects.get(idPe_je=idPe_je)
    idPl_je=request.POST["idPl_je"]
    platilloSeleccionado=Platillo.objects.get(idPl_je=idPl_je)
    
    cantidad_je=request.POST["cantidad_je"]
    descripcion_je=request.POST["descripcion_je"]
    total_je=request.POST["total_je"]
    nuevoDetalle=Detalle.objects.create(pedido=pedidoSeleccionado,
    platillo=platilloSeleccionado,cantidad_je=cantidad_je,
    descripcion_je=descripcion_je,total_je=total_je)
    messages.success(request,'Detalle guardado correctamente')
    return redirect('listadoDetalle')

def eliminarDetalle(request,idD_je):
    detalleEliminar=Detalle.objects.get(idD_je=idD_je)
    detalleEliminar.delete()
    messages.warning(request,'Detalle eliminado correctamente')
    return redirect('listadoDetalle')

def editarDetalle(request,idD_je):
    detalleEditar=Detalle.objects.get(idD_je=idD_je)
    pedidosBdd=Pedido.objects.all()
    platillosBdd=Platillo.objects.all()
    return render(request, 'editarDetalle.html',{
        'detalle':detalleEditar, 
        'pedidos':pedidosBdd,
        'platillos':platillosBdd
        })

def procesarActualizacionDetalle(request):
    idD_je=request.POST["idD_je"]

    idPe_je=request.POST["idPe_je"]
    pedidoSeleccionado=Pedido.objects.get(idPe_je=idPe_je)
    idPl_je=request.POST["idPl_je"]
    platilloSeleccionado=Platillo.objects.get(idPl_je=idPl_je)
    
    cantidad_je=request.POST["cantidad_je"]
    descripcion_je=request.POST["descripcion_je"]
    total_je=request.POST["total_je"]
    #insertar datos mediante el ORM de django
    detalleEditar=Detalle.objects.get(idD_je=idD_je)
    detalleEditar.cantidad_je=cantidad_je
    detalleEditar.descripcion_je=descripcion_je
    detalleEditar.total_je=total_je
    detalleEditar.pedido=pedidoSeleccionado
    detalleEditar.platillo=platilloSeleccionado
    detalleEditar.save()
    messages.success(request,'Pedido actualizado correctamente')
    return redirect('listadoDetalle')


#--------INGREDIENTE----
def listadoIngrediente(request):
    ingredientesBdd=Ingrediente.objects.all()
    return render(request, 'listadoIngrediente.html',{'ingredientes':ingredientesBdd})

def guardarIngrediente(request):
    
    nombre_je=request.POST["nombre_je"]
    descripcion_je=request.POST["descripcion_je"]
    fecha_caducidad_je=request.POST["fecha_caducidad_je"]
    cantidad_je=request.POST["cantidad_je"]
    imagen_je=request.FILES.get("imagen_je")
    nuevoIngrediente=Ingrediente.objects.create(nombre_je=nombre_je,
    descripcion_je=descripcion_je,fecha_caducidad_je=fecha_caducidad_je,
    cantidad_je=cantidad_je,imagen_je=imagen_je)
    messages.success(request,'Ingrediente guardado correctamente')
    return redirect('listadoIngrediente')

def eliminarIngrediente(request,idI_je):
    ingredienteEliminar=Ingrediente.objects.get(idI_je=idI_je)
    ingredienteEliminar.delete()
    messages.warning(request,'Ingrediente eliminado correctamente')
    return redirect('listadoIngrediente')

def editarIngrediente(request,idI_je):
    ingredienteEditar=Ingrediente.objects.get(idI_je=idI_je)
    return render(request, 'editarIngrediente.html',{'ingrediente':ingredienteEditar})

def procesarActualizacionIngrediente(request):
    idI_je=request.POST["idI_je"]
    nombre_je=request.POST["nombre_je"]
    descripcion_je=request.POST["descripcion_je"]
    fecha_caducidad_je=request.POST["fecha_caducidad_je"]
    cantidad_je=request.POST["cantidad_je"]
    imagen_je=request.FILES.get("imagen_je")
    #insertar datos mediante el ORM de django
    ingredienteEditar=Ingrediente.objects.get(idI_je=idI_je)
    ingredienteEditar.nombre_je=nombre_je
    ingredienteEditar.descripcion_je=descripcion_je
    ingredienteEditar.fecha_caducidad_je=fecha_caducidad_je
    ingredienteEditar.cantidad_je=cantidad_je
    ingredienteEditar.imagen_je=imagen_je
    ingredienteEditar.save()
    messages.success(request,'Ingrediente actualizado correctamente')
    return redirect('listadoIngrediente')

#----------RECETA---------

def listadoReceta(request):
    recetasBdd=Receta.objects.all()
    ingredientesBdd=Ingrediente.objects.all()
    platillosBdd=Platillo.objects.all()
    return render(request, 'listadoReceta.html',{
        'recetas':recetasBdd, 
        'ingredientes':ingredientesBdd,
        'platillos':platillosBdd
        })

def guardarReceta(request):
    idI_je=request.POST["idI_je"]
    ingredienteSeleccionado=Ingrediente.objects.get(idI_je=idI_je)
    idPl_je=request.POST["idPl_je"]
    platilloSeleccionado=Platillo.objects.get(idPl_je=idPl_je)
    
    preparacion_je=request.POST["preparacion_je"]
    dificultad_je=request.POST["dificultad_je"]
    porciones_je=request.POST["porciones_je"]
    nuevoReceta=Receta.objects.create(ingrediente=ingredienteSeleccionado,
    platillo=platilloSeleccionado,preparacion_je=preparacion_je,
    dificultad_je=dificultad_je,porciones_je=porciones_je)
    messages.success(request,'Receta guardado correctamente')
    return redirect('listadoReceta')

def eliminarReceta(request,idR_je):
    recetaEliminar=Receta.objects.get(idR_je=idR_je)
    recetaEliminar.delete()
    messages.warning(request,'Receta eliminado correctamente')
    return redirect('listadoReceta')

def editarReceta(request,idR_je):
    recetaEditar=Receta.objects.get(idR_je=idR_je)
    ingredientesBdd=Ingrediente.objects.all()
    platillosBdd=Platillo.objects.all()
    return render(request, 'editarReceta.html',{
        'receta':recetaEditar, 
        'ingredientes':ingredientesBdd,
        'platillos':platillosBdd
        })

def procesarActualizacionReceta(request):
    idR_je=request.POST["idR_je"]

    idI_je=request.POST["idI_je"]
    ingredienteSeleccionado=Ingrediente.objects.get(idI_je=idI_je)
    idPl_je=request.POST["idPl_je"]
    platilloSeleccionado=Platillo.objects.get(idPl_je=idPl_je)
    
    preparacion_je=request.POST["preparacion_je"]
    dificultad_je=request.POST["dificultad_je"]
    porciones_je=request.POST["porciones_je"]
    #insertar datos mediante el ORM de django
    recetaEditar=Receta.objects.get(idR_je=idR_je)
    recetaEditar.preparacion_je=preparacion_je
    recetaEditar.dificultad_je=dificultad_je
    recetaEditar.porciones_je=porciones_je
    recetaEditar.ingrediente=ingredienteSeleccionado
    recetaEditar.platillo=platilloSeleccionado
    recetaEditar.save()
    messages.success(request,'Receta actualizado correctamente')
    return redirect('listadoReceta')





