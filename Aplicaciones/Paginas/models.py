from django.db import models

# Create your models here.
class Provincia(models.Model):
    idP_je=models.AutoField(primary_key=True)
    nombre_je=models.CharField(max_length=150)
    capital_je=models.TextField()
    imagen_je=models.FileField(upload_to='provincias', null=True, blank=True)
    def __str__(self):
        fila="{0}: {1} "
        return fila.format(self.id, self.nombre, self.descripcion)

class Cliente(models.Model):
    idCl_je=models.AutoField(primary_key=True)
    cedula_je=models.CharField(max_length=10)
    apellido_je=models.CharField(max_length=150)
    nombre_je=models.CharField(max_length=150)
    correo_je=models.EmailField()
    telefono_je=models.CharField(max_length=10)
    provincia=models.ForeignKey(Provincia, null=True, blank=True, on_delete=models.PROTECT)

class Pedido(models.Model):
    idPe_je=models.AutoField(primary_key=True)
    fecha_je=models.DateField()
    cliente=models.ForeignKey(Cliente, null=True, blank=True, on_delete=models.PROTECT)

class Tipo(models.Model):
    idT_je=models.AutoField(primary_key=True)
    tipo_je=models.CharField(max_length=150)
    imagen_je=models.FileField(upload_to='tipos', null=True, blank=True)

class Platillo(models.Model):
    idPl_je=models.AutoField(primary_key=True)
    nombre_je=models.CharField(max_length=150)
    descripcion_je=models.CharField(max_length=150)
    precio_je=models.CharField(max_length=150)
    imagen_je=models.FileField(upload_to='platillos', null=True, blank=True)
    tipo=models.ForeignKey(Tipo, null=True, blank=True, on_delete=models.PROTECT)

class Detalle(models.Model):
    idD_je=models.AutoField(primary_key=True)
    cantidad_je=models.CharField(max_length=10)
    descripcion_je=models.CharField(max_length=150)
    total_je=models.CharField(max_length=150)
    pedido=models.ForeignKey(Pedido, null=True, blank=True, on_delete=models.PROTECT)
    platillo=models.ForeignKey(Platillo, null=True, blank=True, on_delete=models.PROTECT)

class Ingrediente(models.Model):
    idI_je=models.AutoField(primary_key=True)
    nombre_je=models.CharField(max_length=150)
    descripcion_je=models.CharField(max_length=150)
    fecha_caducidad_je=models.DateField()
    cantidad_je=models.EmailField()
    imagen_je=models.FileField(upload_to='ingredientes', null=True, blank=True)

class Receta(models.Model):
    idR_je=models.AutoField(primary_key=True)
    preparacion_je=models.CharField(max_length=200)
    dificultad_je=models.CharField(max_length=150)
    porciones_je=models.CharField(max_length=150)
    ingrediente=models.ForeignKey(Ingrediente, null=True, blank=True, on_delete=models.PROTECT)
    platillo=models.ForeignKey(Platillo, null=True, blank=True, on_delete=models.PROTECT)



